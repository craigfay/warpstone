/**
 * Integration tests the create subcommand module's functions with side effects
 * 
 */

/**
 * Dependencies
 */
const { existsSync } = require('fs');
const { execSync } = require('child_process');
const create = require('../../cli/subcommands/create');

describe('create.run()', () => {

  beforeAll(() => {
    // The test directory should not exist yet
    expect(existsSync('_testdir')).toBe(false);
  });

  test('A warpstone project can be created with create.run()', () => {
    create.run('_testdir', false);
    // Check for core files to exist
    expect(existsSync('_testdir')).toBe(true);
    expect(existsSync('_testdir/index.js')).toBe(true);
    expect(existsSync('_testdir/package.json')).toBe(true);
    expect(existsSync('_testdir/config.js')).toBe(true);
    expect(existsSync('_testdir/boundaries')).toBe(true);
  });

  afterAll(() => {
    // Delete the test directory
    execSync('rm -rf _testdir');
    expect(existsSync('_testdir')).toBe(false);
  });
});
