/**
 * Integration tests the warp command's side effects
 * 
 */

/**
 * Dependencies
 */
const { existsSync } = require('fs');
const { execSync } = require('child_process');

describe('warp create', () => {

  beforeAll(() => {
    // The test directory should not exist yet
    expect(existsSync('../_testdir')).toBe(false);
  });

  test('A warpstone project can be created with warp create <projectname>', () => {
    execSync('warp create _testdir', { cwd: '../' })
    // Check for core files to exist
    expect(existsSync('../_testdir')).toBe(true);
    expect(existsSync('../_testdir/index.js')).toBe(true);
    expect(existsSync('../_testdir/package.json')).toBe(true);
    expect(existsSync('../_testdir/config.js')).toBe(true);
    expect(existsSync('../_testdir/boundaries')).toBe(true);
  });

  afterAll(() => {
    // Delete the test directory
    execSync('rm -rf _testdir', { cwd: '../' });
    expect(existsSync('../_testdir')).toBe(false);
  });
});
