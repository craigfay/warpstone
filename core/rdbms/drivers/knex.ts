/**
 * Dependencies
 */
const knex = require('knex');
const fs = require('fs');
import { RdbmsConfig, ModuleExport } from '../interface';

/**
 * Export Object
 */
export let rdbms:ModuleExport;
rdbms = {
  connect: function(config) {
    return knex(config);
  },
  migrate: {
    make: async function(name, config:RdbmsConfig) {
      await writeMigrationFile(name, config.migrations.directory);
      process.exit();
    },
    latest: async function(config) {
      await knex(config).migrate.latest()
      process.exit();
    },
    rollback: async function(config, all=false) {
      await knex(config).migrate.rollback(all);
    }
  }
};

/**
 * Write a new db migration file
 * @param {string} name the name of the migration
 * @param {string} dir the absolute path to the migrations directory
 */
async function writeMigrationFile(name, dir) {
  const filename = `${yyyymmddhhmmss()}_${name}.js`;
  fs.mkdirSync(dir, { recursive: true });
  fs.writeFileSync(`${dir}/${filename}`, migrationTemplate());
}

/**
 * Get a string representation of the current time in yyyymmddhhmmss format
 * @returns {string}
 */
function yyyymmddhhmmss() {
  const d = new Date();
  return d.getFullYear().toString() + padDate(d.getMonth() + 1) + padDate(d.getDate()) + padDate(d.getHours()) + padDate(d.getMinutes()) + padDate(d.getSeconds());
}

/**
 * Get a date object in the correct format, without requiring a library
 * @param {string} segment 1 or 2 char representation of a year, month, day, hour, or minute
 * @return {string}
 */
function padDate(segment) {
  segment = segment.toString();
  return segment[1] ? segment : `0${segment}`;
} 

/**
 * Return the string content of a migration file
 * @return {string}
 */
function migrationTemplate() {
  return `
    exports.up = function(database) {

    };

    exports.down = function(database) {

    };
  ` // Remove 4 leading spaces from each line
  .split('\n').map(line => line.replace('    ', '')).join('\n');
}
