export interface ModuleExport {
  connect: (config) => any;
  migrate: {
    make: (name, config) => any;
    latest: (config) => any;
    rollback: (config, all) => any;
  }
}

export interface RdbmsConfig {
  client: string;
  connection: object;
  migrations: {
    directory: string;
  },
};