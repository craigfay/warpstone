/**
 * The Koa driver for the http module
 */

/**
 * Dependencies
 */
const Koa = require('koa');
const cors = require('@koa/cors');
const bodyParser = require('koa-bodyparser');
const serve = require('koa-static');

import { HttpRequest, HttpRequestHandler, ModuleExport } from '../interface';
import { response, urlMatch } from '../util';

interface CTX {
  url : string;
  method : string;
  query : object;
  headers : object
  request : { body : string }
  status : number;
  set : (key : string, value : any) => any;
  response : { body : string | object },
  cookies : { get : any, set : any },
}

/**
 * Adapters
 */

function cookieProxy(ctx : CTX): any {
  return new Proxy({}, {
    get: function(obj, prop) {
      try {
        return ctx.cookies.get(prop);
      } catch (e) {
        return undefined;
      }
    }
  });
}

interface HttpServerConfig {
  port? : number,
  staticDir? : string,
  routes? : any,
  callback? : () => any,
}

let routes = [];

function route(pattern, handler): void {
  let convertedHandler = async function(ctx : CTX, next : () => any) {
    // Check that the incoming request url matches the route pattern
    const segments = urlMatch(pattern, ctx.method, ctx.url);
    
    if (segments) {
      // Build an HttpRequest
      const request = {
        method: ctx.method,
        url: ctx.url,
        segments,
        querystring: ctx.query,
        headers: ctx.headers,
        body: ctx.request.body,
        cookies: cookieProxy(ctx),
      };

      // Get an HttpResponse from the handler
      const response = await handler(request);
      if (response) {
        // Set Status Code
        ctx.status = response.status;
        // Set Cookies
        Object.keys(response.cookies).forEach(key => {
          ctx.cookies.set(key, response.cookies[key]);
        })
        // Set Headers
        const headerKeys = Object.keys(response.headers);
        headerKeys.forEach(function(key) {
          const value = response.headers[key];
          ctx.set(key, value);
        });
        // Set Body
        ctx.response.body = response.body;
        return; // Send Response
      }
    }
    await next();
  }
  // Push the route onto the queue
  routes.push(convertedHandler);
}

/**
 * @todo Document this
 * @param config
 */
function start(config : HttpServerConfig = {}) {
  
  /**
   * Instantiate Koa
   */
  const app = new Koa();

  /**
   * Allow Cross Origin
   */
  app.use(cors());

  /**
   * Body Parser
   */
  app.use(bodyParser())

  /**
   * Specify static file directory
   */
  if (config.staticDir) {
    app.use(serve(config.staticDir));
  }

  /**
   * Apply routes as middleware
   */
  routes.forEach(function(route : HttpRequestHandler) {
    app.use(route);
  })

  const FgYellow = "\x1b[33m";
  const Reset = "\x1b[0m";

  app.listen(config.port);
}

interface Server {
  start : (config:HttpServerConfig) => void;
  route : (pattern:String, handler:() => any) => void;
}

/**
 * Create Server
 */
function createServer(config:object) : Server {
  return {
    start: () => start(config),
    route: route,
  }
}

/**
 * Export Structure
 */
export const http:ModuleExport = {
  response,
  createServer,
}
