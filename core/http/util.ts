/**
 * Dependencies
 */
const UrlPattern = require('url-pattern');

import { HttpResponse } from './interface';

/**
 * Send an HttpResponse shaped object to the client
 */
export function response(response : HttpResponse) {
  return {
    status: sanitizeStatus(response.status),
    headers: sanitizeHeaders(response.headers),
    body: sanitizeBody(response.body),
    cookies: sanitizeCookies(response.cookies),
  }
}

/**
 * Sanitize http status codes, defaulting to 200
 * Only accepts whole numbers between 100 and 999
 * @param code
 */
function sanitizeStatus(code : any): number {
  if (typeof code == 'number' && code % 1 == 0) {
    if (code >= 100  && code <= 999) {
      return code;
    }
  }
  return 200;
}

/**
 * Sanitize http response headers
 * @todo maybe lowercase keys/values
 * @param headers
 */
function sanitizeHeaders(headers : any): object {
  const defaultHeaders = {
    'content-type': 'text/plain; charset=utf-8',
  }
  return typeof headers == 'object' ? headers : defaultHeaders;
}

/**
 * Sanitize http response body
 * JSON encodes non-strings 
 * @param body
 */
function sanitizeBody(body : any): string {
  return typeof body == 'string' ? body : JSON.stringify(body);
}

/**
 * Sanitize http response cookies
 * @param cookies
 */
function sanitizeCookies(cookies : object): object {
  return typeof cookies == 'object' ? cookies : {};
}

/**
 * Determine if an incoming request matches a handler pattern
 * @todo Document this
 * @param {string} pattern e.g. "POST /users/admin"  
 * @param url
 */
export function urlMatch(pattern : string, requestMethod : string, requestUrl : string): object | null {
  // Return null for non matching methods
  const [handlerMethod, handlerUrl] = pattern.split(' ');
  if (handlerMethod !== requestMethod) {
    return null;
  }

  const [segments] = requestUrl.split('?'); // Split segments from querystring
  return new UrlPattern(handlerUrl).match(segments);
}
