export interface Start {
  (config:any) : void;
}
export interface SetWithExpiration {
  (key:string, value:string, minutes:number) : void;
}
export interface Set {
  (key:string, value:string) : void;
}
export interface Unset {
  (key:string) : void;
}
export interface Get {
  (key:string) : any;
}
export interface GetAll {
  () : any;
}

export interface Cache {
  start:Start;
  setWithExpiration:SetWithExpiration;
  set:Set;
  unset:Unset;
  get:Get;
  getAll:GetAll;
}

export interface CreateInMemory {
  () : Cache;
}

export interface ModuleExport {
  createInMemory:CreateInMemory;
}
