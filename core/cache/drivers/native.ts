/**
 * Dependencies
 */
import {
  ModuleExport,
  Cache,
  Start,
  SetWithExpiration,
  Set,
  Unset,
  Get,
  GetAll,
} from '../interface';

function createInMemory():Cache {
  return {
    start() {
      this._cache = {};
    },
    setWithExpiration: function(key, value, minutes) {
      this._cache[key] = value;
      setTimeout(function() {
        delete this._cache[key];
      }, minutes * 60 * 1000);
    },
    set: function(key, value) {
      this._cache[key] = value;
    },
    unset: function(key) {
      delete this._cache[key];
    },
    get: function(key) {
      return this._cache[key];
    },
    getAll: function() {
      return this._cache;
    },
  }
}

/**
 * Export
 */
export let cache:ModuleExport;
cache = {
  createInMemory,
}
