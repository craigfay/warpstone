/**
 * Dependencies
 */
const vm = require('vm');
const _repl = require('repl');

const FgRed = '\x1b[31m';
const FgGreen = '\x1b[32m';
const FgYellow = '\x1b[33m';
const FgBlue = '\x1b[34m';
const Reset = '\x1b[0m';

interface ReplConfig {
  authentication?: (str:String) => any;
  commands?: Object;
}

/**
 * Evaluate Console Input
 * @param {Object} commands
 * @param {*} help
 */
function start(config:ReplConfig={}) {

  let identity;

  const prompt = ' > ';
  const replServer = _repl.start({
    prompt,
    useColors: true,
    eval: async function(buffer) {

      if (!identity && config.authentication) {
        console.log('Use `.login <email> <password>` to gain access');
      } else {
        console.log(await eval(buffer));
      }
      this.displayPrompt();
    }
  });
  
  replServer.defineCommand('status', function() {});
  replServer.defineCommand('login', async function(str) {
    identity = await config.authentication(str);
    this.displayPrompt();
  });
  replServer.defineCommand('help', function() {});

  replServer.defineCommand('exit', function() {
    process.exit();
  });

  // Create a _repl context that doesn't have access to globals
  Object.defineProperty(replServer, 'context', {
    configurable: false,
    enumerable: true,
    value: vm.createContext(),
  })

  Object.defineProperty(replServer.context, 'commands', {
    configurable: false,
    enumerable: true,
    value: config.commands, 
  });
}

function error(message: string) {
  console.error(FgRed + message + Reset);
}

function success(message: string) {
  console.info(FgGreen + message + Reset);
}

function clear() {
  console.clear();
}

// Create a repl instance
function create(config) {
  return {
    start: () => start(config),
    error,
    success,
    clear,
  };
}

/**
 * Exports
 */
export const repl = {
  create,
}
