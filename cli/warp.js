#!/usr/bin/env node

/**
 * Handle use of the warp command from the console
 */

/**
 * Dependencies
 */
const create = require('./subcommands/create');
const { migrate, makeMigration } = require('./subcommands/db');

const CLI_VERSION = '0.1.1';

const input = process.argv.slice(2);

// Separate flags and args
const flags = input.filter(arg => /^-/.test(arg));
const args = input.filter(arg => !/^-/.test(arg));

// The subcommand is the first argument passed in after `warp`
const subcommand = args[0] ? args[0] : '';

async function main() {
  /**
   * `warp`
   */
  if (subcommand == '') {
    if (0 == flags.length) {
      return;
    }
    // Valid
    if (flags.includes('-v') || flags.includes('--version')) {
      return console.log(CLI_VERSION);
    }
    return;
  }

  /**
   * `warp create`
   */
  else if (subcommand == 'create') {
    let appname = args[1] ? args[1] : '';
    // Valid
    if ('' != appname) {
      return create.run(appname);
    }
    return console.log('Usage: warp create <projectname>');
  }

  /**
   * `warp db.migrate`
   */
  else if (subcommand == 'db.migrate') {
    let name = args[1];
    await migrate(name);
  }

  /**
   * `warp db.makeMigration`
   */
  else if (subcommand == 'db.makeMigration') {
    let name = args[1];
    await makeMigration(name);
  }
}

main();