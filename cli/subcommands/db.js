/**
 * Dependencies
 */
const { rdbms } = require('../../build');

function getConfig() {
  const config = require(process.cwd() + '/config.js');
  if (!config) throw new Error('Cannot find config.js')
  return config;
}

// Perform the most recent migration, unless another is specified
async function migrate(name) {
  const { rdbmsConfig } = getConfig();
  rdbms.migrate.latest(rdbmsConfig);
}

// Create a new migration file
async function makeMigration(name) {
  const { rdbmsConfig } = getConfig();
  rdbms.migrate.make(name, rdbmsConfig);
}

// Roll back migrations: one migration at a time by default
async function rollback(all=false) {
  const { rdbmsConfig } = getConfig();
  rdbms.migrate.rollback(rdbmsConfig, all);
}

module.exports = {
  migrate,
  makeMigration,
  rollback,
}