/**
 * Unit tests the create subcommand module
 * 
 */

/**
 * Dependencies
 */
const create = require('./create');

describe('baseFilePath()', () => {
  test('template files will lose everything left of /.templates/', () => {
    const filepath = create.baseFilePath('location/.templates/example.js', 'project');
    expect(filepath.indexOf('location')).toBe(-1);
  });
  
  test('template files will lose /.templates/ as well', () => {
    const filepath = create.baseFilePath('location/.templates/example.js', 'project');
    expect(filepath.indexOf('.templates')).toBe(-1);
  });
  
  test('Everything left of and including /.templates/ should be replaced with /{{ appname }}/', () => {
    expect(create.baseFilePath('/.templates/example.js', 'project')).toEqual('project/example.js');
  });
  
  test('-tpl file extensions should be removed', () => {
    expect(create.baseFilePath('/.templates/index.html-tpl', 'project')).toEqual('project/index.html');
  })
  
  test('Backslashes should be acceptable path delimiters', () => {
    expect(create.baseFilePath('location\\.templates\\example.js', 'project')).toEqual('project\\example.js');
  });
});
