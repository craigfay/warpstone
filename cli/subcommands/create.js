/* eslint-disable no-console */

/**
 * Create a new warpstone project
 * This module is a helper for warp.js, and should never be accessed directly
 *
 */

/**
 * Dependencies
 */
const fs = require('fs');
const path = require('path');
const { execSync } = require('child_process');
const { resolve } = require('path');
const glob = require('glob');
const esc = require('../escapes');

/**
 * Wrap console output so that it is suppressed during testing
 * @param {String} output
 */
const inform = (output) => {
  if (process.env.NODE_ENV !== 'test') {
    console.info(output);
  }
};

/**
 * Generate the new content of a file based on its template and the new content
 * @param {String} template The contents of a template file with unreplaced curly tags
 * @param {String} replacement The content that will be inserted in place of the curly tags
 * @returns {String}
 * @todo Make extensible for multiple types of tags
 */
const replaceTags = (template, replacement) => {
  const replaceable = /\{\{ appname \}\}/g;
  return template.replace(replaceable, replacement);
};

/**
 * Generate the new filepath for template files
 * @param {String} path The path name of the template file
 * @param {String} appname The application name, which will be a top level directory
 * @returns {String}
 */
const baseFilePath = (filepath, appname) => filepath
  .replace(/.*\.templates/g, appname)
  .replace('-tpl', '');

/**
 * Given an app name, generate starter files for an application
 * @param {Array} templates A list of paths to template files
 * @param {String} appname The name of the project being initialized
 * @returns {undefined}
 */
const generateBaseFiles = (templates, appname) => {
  // Iterate over templates
  templates.map((filepath) => {
    // Initialize helper values
    const template = fs.readFileSync(filepath).toString();
    const replaced = replaceTags(template, appname);
    const newpath = baseFilePath(filepath, appname);

    // Create directories that don't yet exist
    newpath.split(/[/\\]/).reduce((left, right) => {
      if (!fs.existsSync(left)) {
        fs.mkdirSync(left);
      }
      // Join the path segments
      return resolve(left, right);
    });
    return fs.writeFileSync(newpath, replaced);
  });
};

/**
 * Run `npm install --silent` inside of the newly created project
 * @param {String} appname The directory containing the new project
 * @returns {Buffer | String}
 */
const installDependencies = (appname) => {
  execSync('npm install --silent', { cwd: `${appname}` });
};

/**
 * Return a list of filepaths to new project templates
 * @returns {Array}
 */
const getTemplatePaths = () => {
  // Define the path to template files
  const templateDir = path.resolve(__dirname, '../../.templates');
  // Get all template paths within the template directory
  return glob.sync(`${templateDir}/**/*-tpl`, { nodir: true, dot: true });
};

/**
 * The console output to be shown when a project is created successfully
 * @param {String} appname
 */
const successMessage = appname => ''
  + `${esc.FgGreen}Success! ${esc.Reset}`
  + 'Run the following to get started:\n\n'
  + `  $ ${esc.FgYellow}cd ${appname}${esc.Reset}\n`
  + `  $ ${esc.FgYellow}npm install${esc.Reset}\n`
  + `  $ ${esc.FgYellow}node index${esc.Reset}\n`;

/**
 * A wrapper for high level functions within the module
 * @param {String} appname The name of the project being initialized
 * @param {Boolean} dependencies Whether or not to install project dependencies
 */
const run = (appname, dependencies = false) => {
  inform(`${esc.Dim}Getting template paths...${esc.Reset}`);
  const templates = getTemplatePaths();

  inform(`${esc.Dim}Generating base files...${esc.Reset}`);
  generateBaseFiles(templates, appname);

  if (dependencies) {
    inform(`${esc.Dim}Installing dependencies...${esc.Reset}\n`);
    installDependencies(appname);
  }

  inform(successMessage(appname));
};

/**
 * Exports
 */
if (process.env.NODE_ENV !== 'test') {
  module.exports = { run };
} else {
  module.exports = {
    run,
    baseFilePath,
    getTemplatePaths,
  };
}
