/**
 * Dependencies
 */
const { db } = require('../build');

function getConfig() {
    const config = require(process.cwd() + '/config.js');
    if (!config) throw new Error('Cannot find config.js')
    return config;
}

// Perform the most recent migration, unless another is specified
async function migrate(name) {
    const config = getConfig().db;
    db.migrate.latest(config);
}

// Create a new migration file
async function makeMigration(name) {
    const config = getConfig().db;
    db.migrate.make(name, config);
}

module.exports = {
    migrate,
    makeMigration,
}